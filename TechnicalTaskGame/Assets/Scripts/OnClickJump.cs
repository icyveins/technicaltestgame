using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickJump : MonoBehaviour
{
    public PlayerController playerController;

    public void ButtonClick()
    {
        playerController.SwitchState(playerController.jumpState);
    }
}
