using UnityEngine;

public class IdleState : PlayerState
{
    public override void EnterState(PlayerController player)
    {
        //basic state
        Debug.Log("Heroine is Enter Idle");
    }

    public override void UpdateState(PlayerController player)
    {
        
    }

    public override void ExitState(PlayerController player)
    {

    }
}
