using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinePoint : MonoBehaviour
{
    public LineRenderer line;
    private Vector3 _startMousePos;
    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        
        line.startWidth = 0.2f;
        line.endWidth = 0.2f;
        line.positionCount = 2;

    }

    //this will me called in fire and jump

    public void drawLineDirection()
    {
        _startMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        line.SetPosition(1, _startMousePos);
        line.SetPosition(0, player.position);
    }
}
