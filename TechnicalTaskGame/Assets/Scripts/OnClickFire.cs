using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickFire : MonoBehaviour
{
    public PlayerController playerController;

    public void ButtonClick()
    {
        playerController.SwitchState(playerController.fireballState);
    }

}

