using UnityEngine;


public class FireballState : PlayerState
{
    private float _nextFire = 0.0f;
    private float fireRate = 0.5f;
    private Vector3 _mousePos;
    private Camera _mainCam;
    private LinePoint _line;
    SpriteRenderer sr;

    public override void EnterState(PlayerController player)
    {
        Debug.Log("Heroine is in Enter Fireball");
        player.animator.SetBool("Jumping", false);
        _mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        sr = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>();
        _line = GameObject.FindGameObjectWithTag("Line").GetComponent<LinePoint>();
    }

    public override void UpdateState(PlayerController player)
    {

        if (Input.GetButtonDown("Fire1"))
        {
            //checking if player is alive he can shoot fireballs and checking time between shoots
            if (player.Alive() != 0 && Time.time > _nextFire)
            {
                _nextFire = Time.time + fireRate;
                player.animator.SetBool("Fire", true);
                _mousePos = _mainCam.ScreenToWorldPoint(Input.mousePosition);
                Vector3 rotation = _mousePos - player.rotatepoint.position;
                float rotZ = Mathf.Atan2(rotation.y, rotation.x) * Mathf.Rad2Deg;
                player.rotatepoint.rotation = Quaternion.Euler(0, 0, rotZ);
                player.InstatiatonFireball();
                _line.drawLineDirection();
                //rotating player in shoot side
                if (rotation.x > 0.1)
                {
                    sr.flipX = false;
                }
                else
                {
                    sr.flipX = true;
                }


            }

        }
    }   

    public override void ExitState(PlayerController player)
    {

    }
}
