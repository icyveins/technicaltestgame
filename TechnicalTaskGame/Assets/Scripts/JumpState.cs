using UnityEngine;

public class JumpState : PlayerState
{
    Vector2 lastClickedPos;
    bool moving;
    private LinePoint _line;

    public override void EnterState(PlayerController player)
    {
        Debug.Log("Heroine is in Enter Jump");
        player.animator.SetBool("Fire", false);
        _line = GameObject.FindGameObjectWithTag("Line").GetComponent<LinePoint>();
    }

    public override void UpdateState(PlayerController player)
    {
        //player can jump only if grounded (isGrounded in PlayerController)
        if (Input.GetButtonDown("Fire1") && player.isGrounded())
        {
            lastClickedPos = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
            moving = true;
            //Debug.Log(lastClickedPos);
            if (player.Alive() > 0) { 
            _line.drawLineDirection();
            }
        }
        //moving is avaliable only if person alive (make this check later!!!)
        if ((moving && (Vector2)player.transform.position != lastClickedPos) && player.Alive() > 0)
        {
            player.animator.SetBool("Fire", false);
            player.animator.SetBool("Falling", false);
            float step = player.jumpForce * Time.deltaTime;
            player.transform.position = Vector2.MoveTowards(player.transform.position, lastClickedPos, step);
            player.animator.SetBool("Jumping", true);
        } else {
                moving = false;
                player.animator.SetBool("Jumping", false);
                player.animator.SetBool("Falling", true);
        }
        

    }

    public override void ExitState(PlayerController player)
    {

    }
}
