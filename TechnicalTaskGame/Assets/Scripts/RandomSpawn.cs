using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject enemyPrefab;
    private int _countEnemies = 0;
    // Start is called before the first frame update

    void Start()
    {
            //loop to check if there enemies, if first loop didnt spawned enemies
            while (_countEnemies == 0) { 
            for (int i = 0; i < spawnPoints.Length; i++)
            {
                bool spawnOrNot = Random.value > 0.5;

                if (spawnOrNot)
                {
                    Instantiate(enemyPrefab, spawnPoints[i].position, transform.rotation);
                    _countEnemies++;
                }
                else
                {
                    continue;
                }

            }
        }
        //Debug.Log(_countEnemies); to check number of enemies
    }



    // Update is called once per frame
    void Update()
    {
        
    }
}
