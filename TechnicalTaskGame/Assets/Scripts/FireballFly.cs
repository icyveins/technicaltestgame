using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballFly : MonoBehaviour
{
    
    private float _speed = 20f;
    public Rigidbody2D rb;
    public GameObject fireballEnd;
    public GameObject enemyPrefab;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * _speed;
    }
    void Update()
    {
        //destroying fireball prefab and fireball ending prefab
        Destroy(gameObject, 3);
        Destroy(GameObject.FindGameObjectWithTag("FireGone"), 1);
    }

    //void OnTriggerEnter2D (Collider2D other)
    void OnCollisionEnter2D(Collision2D other)
    {
        //instantiating firebal ending prefab and destroying it with common fireball prefab
        Instantiate(fireballEnd, transform.position, transform.rotation);
        Destroy(gameObject);
        Destroy(GameObject.FindGameObjectWithTag("FireGone"), 1);

    }
}
