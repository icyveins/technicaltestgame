using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 1f;
    public float jumpForce = 1f;
    public LayerMask jumpableGround;
    private int _maxHealth = 2;
    private int _currentHealth;
    public DeathMenu deathMenu;
    public Transform firepoint;
    public Transform rotatepoint; 
    public GameObject FireballPrefab;
    BoxCollider2D coll;
    Rigidbody2D rb;
    SpriteRenderer sr;
    public Animator animator;
    public HealthBar healthBar;
    PlayerState currentState;
    public FireballState fireballState = new FireballState();
    public JumpState jumpState = new JumpState();
    public IdleState idleState = new IdleState();

    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        currentState = idleState;
        currentState.EnterState(this);
        _currentHealth = _maxHealth;
        healthBar.SetMaxHealth(_maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        //mooving with physic
        float movement = Input.GetAxisRaw("Horizontal");
        animator.SetFloat("Speed", Mathf.Abs(movement));

        currentState.UpdateState(this);

        rb.velocity = new Vector2(movement * speed, rb.velocity.y);

        //player cant move and flip while dead
        if (movement < 0 && _currentHealth > 0)
        {
            sr.flipX = true;
            
        } else if (movement > 0 && _currentHealth > 0)
        {
            sr.flipX = false;
        } else if (_currentHealth == 0)
        {
            sr.flipX = false;
        }

        //Debug.Log(isGrounded());

        if (isGrounded())
        {
            animator.SetBool("Falling", false);
        }
    }


    public void SwitchState(PlayerState state)
    {
        currentState = state;
        state.EnterState(this);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //player recieve damage on collision with enemies
        if (other.gameObject.tag == "Enemy")
        {
            _currentHealth -= 1;
            healthBar.SetHealth(_currentHealth);
            if (_currentHealth != 0) {
                animator.SetBool("Jumping", false);
                animator.SetBool("Injured", true);
            //Debug.Log(currentHealth);
            } else
            {
                Die();
            }
        }
    }

    public void AnimationEnded(string anName)
    {
        //to turn off animations
        animator.SetBool(anName, false);

    }

    private void Die()
    {
        rb.bodyType = RigidbodyType2D.Static;
        animator.SetBool("Jumping", false);
        animator.SetTrigger("Death");
        deathMenu.ToggleEndMenu();

    }

    public int Alive()
    {
        return _currentHealth;
    }

    public bool isGrounded()
    {
        return Physics2D.BoxCast(coll.bounds.center, coll.bounds.size, 0f, Vector2.down, .1f, jumpableGround);

    }

    public void InstatiatonFireball()
    {
        Instantiate(FireballPrefab, firepoint.position, firepoint.rotation);
    }   

}
