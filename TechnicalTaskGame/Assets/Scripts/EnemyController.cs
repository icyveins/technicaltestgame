using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Animator animator;
    private int _maxHealth = 2;
    private int _currentHealth;
    public HealthBar healthBar;
    Collider2D col;
    // Start is called before the first frame update
    void Start()
    {
        //set health to enemy
        _currentHealth = _maxHealth;
        healthBar.SetMaxHealth(_maxHealth);
        col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AnimationEnded(string anName)
    {
        animator.SetBool(anName, false);

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //damage if collision with fireball
        if (other.gameObject.tag == "Fireball")
        {
            _currentHealth -= 1;
            healthBar.SetHealth(_currentHealth);
            if (_currentHealth != 0)
            {
                animator.SetBool("EnemyInjured", true);
            }
            else
            {
                //destroying enemy and making collision to false
                Die();
                col.enabled = false;
                Destroy(gameObject, 3);
            }
        }
    }
    private void Die()
    {
        animator.SetTrigger("EnemyDeath");
    }
}
